if require('openmw.core').API_REVISION < 22 then
    error('This mod requires a newer version of OpenMW, please update.')
end

local core = require('openmw.core')
local camera = require('openmw.camera')
local input = require('openmw.input')
local self = require('openmw.self')
local util = require('openmw.util')
local ui = require('openmw.ui')
local nearby = require('openmw.nearby')
local types = require('openmw.types')
local I = require('openmw.interfaces')

local Actor = types.Actor
local Item = types.Item
local Weapon = types.Weapon

local health = Actor.stats.dynamic.health

if I.Camera then
    I.Camera.disableModeControl()
    I.Camera.disableStandingPreview()
    I.Camera.disableThirdPersonOffsetControl()
    I.Camera.disableZoom()
end
camera.setFocalPreferredOffset(util.vector2(0, 0))

local MODE = camera.MODE

local cursor_normal = ui.texture{path = 'textures/tx_cursor.dds'}
local cursor_clicked = ui.texture{path = 'textures/cursor_drop_ground.dds'}
local cursor_item = ui.texture{path = 'textures/OpenNevermind/backpack.png'}
local cursor_sword = ui.texture{path = 'textures/OpenNevermind/sword.png'}
local cursor_bow = ui.texture{path = 'textures/OpenNevermind/bow.png'}
local cursor_talk = ui.texture{path = 'textures/OpenNevermind/envelope.png'}
local cursor_map = ui.texture{path = 'textures/OpenNevermind/map.png'}
local cursor_spell = ui.texture{path = 'textures/OpenNevermind/scroll.png'}

local element = ui.create {
  layer = 'HUD',
  type = ui.TYPE.Image,
  props = {
    size = util.vector2(40, 40),
    resource = cursor_normal,
  },
}

local camPitch = math.rad(80)
local camYaw = 0
local camDist = 800

local cursorPos = util.vector2(0, 0)
local mouseButtonWasPressed = false

local destination = nil
local target = nil
local attackTarget = false
local magicAttack = false
local marksmanAttack = nil

local function checkTarget(ray)
    if ray.hitObject and Actor.objectIsInstance(ray.hitObject) then return true end
    local delta = ray.hitPos - self.position
    return delta.z < 160 or delta.z < 0.5 * delta:length()
end

local pointedDest = nil
local pointedTarget = nil
local pointedCursor = cursor_normal

local function targetClassification(newDest, newTarget)
    pointedCursor = cursor_normal
    if newTarget and newTarget.type ~= types.Static and newTarget.recordId ~= 'chargen boat' then
        pointedTarget = newTarget
        pointedDest = newTarget.position
        if Actor.objectIsInstance(newTarget) then
            if health(newTarget).current <= 0 then
                pointedCursor = cursor_item
            elseif Actor.stance(self) == Actor.STANCE.Weapon then
                pointedCursor = cursor_sword
            elseif Actor.stance(self) == Actor.STANCE.Spell then
                pointedCursor = cursor_spell
            elseif types.NPC.objectIsInstance(newTarget) then
                pointedCursor = cursor_talk
            end
        elseif types.Door.objectIsInstance(newTarget) then
            pointedCursor = cursor_map
        elseif Item.objectIsInstance(newTarget) or types.Container.objectIsInstance(newTarget) then
            pointedCursor = cursor_item
        end
    else
        pointedTarget = nil
        pointedDest = newDest
    end
    if pointedTarget == self.object and pointedCursor ~= cursor_spell then
        pointedCursor = cursor_normal
        pointedTarget = nil
        pointedDest = newDest
    elseif pointedCursor == cursor_sword then
        local item = Actor.equipment(self, Actor.EQUIPMENT_SLOT.CarriedRight)
        local weaponRecord = item and item.type == Weapon and Weapon.record(item)
        if weaponRecord then
            local t = weaponRecord.type
            if t == Weapon.TYPE.MarksmanBow or t == Weapon.TYPE.MarksmanCrossbow or t == Weapon.TYPE.MarksmanThrown then
                pointedCursor = cursor_bow
            end
        end
    end
end

local function findObjectUnderMouse()
    local delta = camera.viewportToWorldVector(cursorPos:ediv(ui.screenSize()))
    local basePos = camera.getPosition() + delta * 50
    local endPos = basePos + delta * 10000
    local res = nearby.castRay(basePos, endPos)
    if not res.hitObject then
        res = nearby.castRenderingRay(basePos, endPos)
    end
    while res.hitPos and not checkTarget(res) do
        local res1 = nearby.castRenderingRay(res.hitPos + delta * 20, endPos)
        local res2 = nearby.castRay(res.hitPos, endPos, {ignore=res.hitObject})
        if not res1.hitPos or (res2.hitPos and (res2.hitPos-basePos):length2() < (res1.hitPos-basePos):length2()) then
            res = res2
        else
            res = res1
        end
    end
    targetClassification(res.hitPos, res.hitObject)
end

local function onFrame(dt)
    if core.isWorldPaused() or camera.getMode() == MODE.FirstPerson then
        element.layout.props.visible = false
        element:update()
        return
    end

    -- Mouse
    local mouseButtonPressed = input.isMouseButtonPressed(1) or input.isControllerButtonPressed(input.CONTROLLER_BUTTON.A)

    local screenSize = ui.screenSize()
    cursorPos = cursorPos + util.vector2(input.getMouseMoveX(), input.getMouseMoveY())
    local controllerCoef = math.min(screenSize.x, screenSize.y) * (dt * 1.5)
    cursorPos = cursorPos + util.vector2(
        input.getAxisValue(input.CONTROLLER_AXIS.MoveLeftRight),
        input.getAxisValue(input.CONTROLLER_AXIS.MoveForwardBackward)) * controllerCoef
    cursorPos = util.vector2(util.clamp(cursorPos.x, 0, screenSize.x), util.clamp(cursorPos.y, 0, screenSize.y))
    element.layout.props.relativePosition = cursorPos:ediv(ui.screenSize())
    findObjectUnderMouse()
    if mouseButtonPressed then
        element.layout.props.resource = cursor_clicked
        element.layout.props.anchor = util.vector2(0.5, 0.5)
    else
        element.layout.props.resource = pointedCursor
        if pointedCursor == cursor_normal then
            element.layout.props.anchor = util.vector2(0.2, 0.0)
        else
            element.layout.props.anchor = util.vector2(0.5, 0.5)
        end
    end
    element.layout.props.visible = true
    element:update()

    -- Mouse click
    if mouseButtonWasPressed and not mouseButtonPressed then
        target = pointedTarget
        destination = pointedDest
        attackTarget = pointedCursor == cursor_sword
        if pointedCursor == cursor_bow and not marksmanAttack then
            marksmanAttack = 1.6
        elseif pointedCursor == cursor_spell then
            magicAttack = true
        end
    end
    mouseButtonWasPressed = mouseButtonPressed

    -- Camera
    if input.isKeyPressed(input.KEY.A) then camYaw = camYaw - dt end
    if input.isKeyPressed(input.KEY.D) then camYaw = camYaw + dt end
    if input.isKeyPressed(input.KEY.W) then camPitch = camPitch + dt end
    if input.isKeyPressed(input.KEY.S) then camPitch = camPitch - dt end
    camYaw = camYaw + input.getAxisValue(input.CONTROLLER_AXIS.LookLeftRight) * dt
    camPitch = camPitch + input.getAxisValue(input.CONTROLLER_AXIS.LookUpDown) * dt
    camPitch = util.clamp(camPitch, 0, math.rad(90))
    if camera.getMode() ~= MODE.Static and camera.getMode() ~= MODE.Preview then
        camera.setMode(MODE.Preview)
    end
    camera.showCrosshair(false)
    camera.setPitch(camPitch)
    camera.setYaw(camYaw)
    camera.setRoll(0)
    camera.setPreferredThirdPersonDistance(camDist)
    if camera.getMode() == MODE.Static then
        local offset = util.transform.rotateZ(camYaw) * util.transform.rotateX(camPitch) * util.vector3(0, -camDist, 0)
        camera.setStaticPosition(camera.getTrackedPosition() + offset)
    end

    -- Movement and attack
    self.controls.sideMovement = 0
    self.controls.movement = 0
    self.controls.yawChange = 0
    self.controls.pitchChange = 0
    self.controls.use = 0
    local yawChange = 0
    local dist = 0
    if target then destination = target.position end
    if destination and target ~= self.object then
        local relDest = destination - self.position - util.vector3(0, 0, 60)
        local destVec = util.vector2(relDest.x, relDest.y):rotate(self.rotation.z)
        yawChange = math.atan2(destVec.x, destVec.y)
        local maxDelta = math.max(math.abs(yawChange), 1) * 2 * dt
        dist = destVec:length()
        self.controls.yawChange = util.clamp(yawChange, -maxDelta, maxDelta)
        self.controls.pitchChange = math.asin(-relDest.z / relDest:length()) - self.rotation.x
    end
    local yawAbs = math.abs(yawChange)
    if marksmanAttack then
        if marksmanAttack > 0 or yawAbs > 0.2 then
            self.controls.use = 1
            marksmanAttack = marksmanAttack - dt
        else
            marksmanAttack = nil
            target = nil
            destination = nil
        end
    elseif magicAttack then
        if yawAbs < 0.1 then
            self.controls.use = 1
            magicAttack = false
            target = nil
            destination = nil
        end
    else
        self.controls.movement = math.min(1, math.min(0.4 / (yawAbs+0.1), dist / 150))
        self.controls.run = self.controls.movement > 0.5
        if not self.controls.run then
            self.controls.movement = self.controls.movement * 2
        end
        if yawAbs > 0.3 and dist < 100 then
            self.controls.movement = 0
        end
        if attackTarget then
            self.controls.use = 1
        end
        if dist < 20 or (target and dist < 100 and yawAbs < 0.3) then
            if target and not attackTarget then
                target:activateBy(self)
            end
            destination = nil
            target = nil
            attackTarget = false
        end
    end
end

return {
    engineHandlers = {
        onFrame = onFrame,
        onInputAction = function(action)
            if core.isWorldPaused() then return end
            if action == input.ACTION.ZoomIn then
                camDist = math.max(50, camDist * 0.95)
            elseif action == input.ACTION.ZoomOut then
                camDist = math.min(5000, camDist / 0.95)
            elseif action == input.ACTION.TogglePOV then
                if camera.getMode() == MODE.FirstPerson then
                    if input.getControlSwitch(input.CONTROL_SWITCH.ViewMode) then
                        camera.setMode(MODE.Static)
                        ui.showMessage("Top-down view")
                    else
                        ui.showMessage("View mode change is not allowed at the moment")
                    end
                elseif camera.getMode() == MODE.Static then
                    camera.setMode(MODE.Preview)
                    ui.showMessage("Top-down view with camera collision")
                else
                    camera.setMode(MODE.FirstPerson)
                end
            end
        end,
        onSave = function()
            return {camPitch = camPitch, camYaw = camYaw, camDist = camDist, cursorPos = cursorPos, mode = camera.getMode()}
        end,
        onLoad = function(data)
            if data and data.camPitch then camPitch = data.camPitch end
            if data and data.camYaw then camYaw = data.camYaw end
            if data and data.camDist then camDist = data.camDist end
            if data and data.cursorPos then cursorPos = data.cursorPos end
            if data and data.mode then camera.setMode(data.mode) end
        end,
    }
}

